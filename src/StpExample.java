import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import org.bouncycastle.util.io.pem.PemReader;
import org.castor.core.util.Base64Decoder;



public final class StpExample
{

    /** The Constant CHARSET. */
    private static final String CHARSET = "utf-8";

    /** The Constant ENCRYPTION_ALGORITHM. */
    private static final String ENCRYPTION_ALGORITHM = "RSA";

    /** The Constant HASH_ENCRIPTION_ALGORITHM. */
    private static final String HASH_ENCRYPTION_ALGORITHM = "SHA256withRSA";


    private StpExample(  )
    {

    }


    public static boolean checkSign( String message, String sign, String keyPath )
    {
        boolean ret = false;

        try
        {

            PublicKey key = StpExample.getKey(keyPath);
            ret = StpExample.verify( message, sign, key);
        } catch ( final IOException | NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | SignatureException e )
        {
            System.out.println("el error" + e.getMessage());
        }

        return ret;
    }



    private static PublicKey getKey( String keyPath )
            throws NoSuchAlgorithmException, IOException, InvalidKeySpecException
    {
        final KeyFactory keyFactory = KeyFactory.getInstance( StpExample.ENCRYPTION_ALGORITHM );
        final PemReader reader = new PemReader( new FileReader( keyPath ) );
        final byte[] pubKey = reader.readPemObject(  ).getContent(  );
        final X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec( pubKey );

        return keyFactory.generatePublic( publicKeySpec );
    }


    private static boolean verify( String message, String sign, PublicKey publicKey )
            throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, UnsupportedEncodingException
    {
        final Signature sig = Signature.getInstance( StpExample.HASH_ENCRYPTION_ALGORITHM );
       sig.initVerify( publicKey );
        sig.update( message.getBytes( StpExample.CHARSET ) );

        final byte[] bytes = Base64Decoder.decode( sign );

        return sig.verify( bytes );

    }

    public static void main(String[] args) {
        System.out.println("test");

        System.out.println("es valida? : " +  StpExample.checkSign("hola",
                "d6V7vQHrldLABsqjKXq+fgOxCsO9oSo5LZSQO6kiWJqP5qiUkKsL2QxLAsfS9iNsE8TevVNYmk6ZIrZ4JOPCNO+6QMkQkqEdqqPl0VhpmNsV+RuVseJhmu68APwTJ+Dt1i+r+OJ0gnwaKlXPOeTNBL2PIW7jTrsNH8mbgzamDzxLa5R/H+Ky8INbGVPMvdLb24d1WEMEW//nTtDSx7/JRaIVaW/Idt8fMxqOb7LaAg/a5Cy/awOBE47dWS+6K6dyzW69E3H1907JiNdx8J25dz3zHlfHNy8eqdO+m4WU4fAJmbZ422Vo4MgPuSZATvTmIy4aWpGZY90gjU70PWNGVg==",
                "src/public.pem"));
    }
}